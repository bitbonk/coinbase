package coinbase

var(
	apiKey string
	apiSecret string
	apiPassphrase string
)

type CredentialsConfig struct{
	Key string
	Secret string
	Passphrase string
}

func SetCredentials(credentials CredentialsConfig) {
	apiKey = credentials.Key
	apiSecret = credentials.Secret
	apiPassphrase = credentials.Passphrase
}



