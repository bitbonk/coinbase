package coinbase

import (
	"net/url"
)

type endpoints struct {
	restUrl      *url.URL
	websocketUrl *url.URL
}

var (
	endpointsSandbox endpoints
	endpointsProd    endpoints
	activeEndpoints  endpoints
)

func init() {
	restSandbox, err := url.Parse("https://api-public.sandbox.pro.coinbase.com")

	if err != nil {
		panic(err)
	}

	websocketSandbox, err := url.Parse("wss://ws-feed-public.sandbox.pro.coinbase.com")

	if err != nil {
		panic(err)
	}

	rest, err := url.Parse("https://api.pro.coinbase.com")

	if err != nil {
		panic(err)
	}

	websocket, err := url.Parse("wss://ws-feed.pro.coinbase.com")

	if err != nil {
		panic(err)
	}

	// Store the actual endpoints.
	endpointsSandbox = endpoints{
		restUrl:      restSandbox,
		websocketUrl: websocketSandbox,
	}

	endpointsProd = endpoints{
		restUrl:      rest,
		websocketUrl: websocket,
	}
}

func EnableSandbox(useSandbox bool) {
	if useSandbox {
		activeEndpoints = endpointsSandbox
	}

	activeEndpoints = endpointsProd
}
