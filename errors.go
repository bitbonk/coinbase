package coinbase

import (
	"bufio"
	"fmt"
)

type (
	UnsignableRequest string

	WebsocketConnectionError struct {
		errMsg string
		body io.readCloser
	}
)

func (u UnsignableRequest) Error() string {
	return string(u)
}

func (wce WebsocketConnectionError) Error() string {
	return fmt.Sprintf(
		"Unable to open websocket connection: %s",
		bufio.NewScanner(wce.body).Text(),
	)
}

const (
	unsignableRequest UnsignableRequest = "secret not set, unable to sign api request"
	websocketConnectionError = "Unable to connect to websocket.",
)


