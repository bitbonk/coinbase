package coinbase

import (
	"net/http"
)

const apiVersion = "2019-09-23"

var (
	defaultHeaders     http.Header
	authHeaders http.Header
)

func init() {
	defaultHeaders = http.Header{
		"Content-Type": []string{"application/json"},
	}

	authHeaders = defaultHeaders.Clone()
}

func setAuthHeaders() {
	authHeaders.Add("CB-ACCESS-KEY", apiKey)
	authHeaders.Add("CB-ACCESS-PASSPHRASE", apiPassphrase)
}
