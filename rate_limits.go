package coinbase

import (
	"time"
)

var (
	publicTicker *time.Ticker
	privateTicker *time.Ticker
	websocketConnectTicker *time.Ticker

	publicThrottle chan time.Time
	privateThrottle chan time.Time
	websocketConnectThrottle chan time.Time
)

func init() {
	publicTicker = time.NewTicker(time.Second / 3)
	privateTicker = time.NewTicker(time.Second / 5)
	websocketConnectTicker = time.NewTicker(time.Second * 4)

	publicThrottle = make(chan time.Time, 6)
	privateThrottle = make(chan time.Time, 10)
	websocketConnectThrottle = make(chan time.Time) //intentionally not buffered.

	go func() {
		for t := range publicTicker.C {
			select {
			case publicThrottle <- t:
			default:
			}
		} // does not exit after tick.Stop()
	}()

	go func() {
		for t := range privateTicker.C {
			select {
			case privateThrottle <- t:
			default:
			}
		} // does not exit after tick.Stop()
	}()

	go func() {
		for t := range websocketConnectTicker.C {
			select {
			case websocketConnectThrottle <- t:
			default:
			}
		}
	}()
}
