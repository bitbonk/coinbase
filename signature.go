package coinbase

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"net/http"
	"strings"
)
/**
fmt.Sprintf(
			"%s%s%s%s",
			timestamp,
			strings.ToUpper(r.Method),
			r.URL.Path,
			bodyBuf.String(),
		),
 */
func generateSig(timestamp, method, uriPath, body string) (string, error) {
	if apiSecret == "" {
		return "", unsignableRequest
	}

	//base64 decode the secret.
	key, err := base64.StdEncoding.DecodeString(apiSecret)
	if err != nil {
		return "", err
	}

	signature := hmac.New(sha256.New, key)
	if _, err = signature.Write(
		[]byte(
			fmt.Sprintf(
				"%s%s%s%s",
				timestamp,
				strings.ToUpper(method), // just to be sure.
				uriPath,
				body,
			),
		),
	); err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(signature.Sum(nil)), nil
}

func generateSigFromRequest(timestamp string, req *http.Request) (string, error) {
	bodyBuf := new(bytes.Buffer)
	if _, bufErr := bodyBuf.ReadFrom(r.Body); bufErr != nil {
		// see golang.org/pkg/bytes and/or ...pkg/io for the error types on this error.
		return "", bufErr
	}

	return generateSig(
		timestamp,
		req.Method,
		req.URL.Path,
		bodyBuf.String(),
	)
}


func signRestRequestHeader(req *http.Request) error {


	timestamp := timestampSec()

	sig, err := generateSigFromRequest(
		timestamp,
		req,
		secret,
	)

	if err != nil {
		return err
	}

	req.Header = defaultAuthHeaders.Clone()
	req.Header.Add("CB-ACCESS-TIMESTAMP", timestamp)
	req.Header.Add("CB-ACCESS-SIGN", sig)

	return nil
}
