package coinbase

import (
	"sync"

	"github.com/gorilla/websocket"
)

var (
	// socketChan - Creating a channel that can be reused in the case of
	// websocket reconnection, so the consumer only has to call 'Connect'
	// again.
	socketChan   chan []byte
	cbSocketConn *websocket.Conn
	socketMutex  sync.RWMutex
)

func init() {
	socketChan = make(chan []byte)
}

func Connect() error {
	<-websocketConnectThrottle
	conn, resp, err := websocket.DefaultDialer.Dial(
		activeEndpoints.websocketUrl.String(),
		nil,
	)

	if err != nil {
		return err
	} else if resp.StatusCode >= 400 {
		return WebsocketConnectionError{
			errMsg: websocketConnectionError,
			body:   resp.Body,
		}
	}

	cbSocketConn = conn

	go func() {
		for {
			socketMutex.RLock()
			messageType, message, err := conn.ReadMessage()
			socketMutex.RUnlock()

			if message != nil {
				socketChan <- message
			}

			if err != nil {
				// handle err?
				socketChan <- []byte("{\"type\": \"websocketConnectionError\"}")
				return
			}

			if messageType == websocket.CloseMessage {
				socketChan <- []byte("{\"type\": \"websocketConnectionClosed\"}")
				return
			}
		}
	}()

	return nil
}

func sendWebsocketMessage(payload WebsocketRequest, authenticate bool) error {
	if authenticate {
		timestamp := timestampSec()
		sig, err := generateSig(
			timestamp,
			"GET",
			"/users/self/verify",
			"",
		)

		if err != nil {
			return err
		}

		payload.authFields.Signature = sig
		payload.authFields.Key = apiKey
		payload.authFields.Passphrase = apiPassphrase
		payload.authFields.Timestamp = timestamp
	}

	socketMutex.Lock()
	defer socketMutex.Unlock()
	return cbSocketConn.WriteJSON(payload)
}
