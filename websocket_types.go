package coinbase

import (
	"encoding/json"
)

type (
	WebsocketChannelName string

	WebsocketSubscriptionChannel struct {
		Name       string   `json:"name"`
		ProductIds []string `json:"product_ids,omitempty"`
	}

	WebsocketRequestType string

	websocketAuth struct {
		Signature  string `json:"signature,omitempty"`
		Key        string `json:"key,omitempty"`
		Passphrase string `json:"passphrase,omitempty"`
		Timestamp  string `json:"timestamp,omitempty"`
	}

	WebsocketRequest struct {
		Type       WebsocketRequestType           `json:"type"`
		ProductIDs []string                       `json:"product_ids,omitempty"` // global productIds for subscription
		Channels   []WebsocketSubscriptionChannel `json:"channels,omitempty"`    //
		// Authenticated Message Fields. @TODO embed these in a non-exposed way.
		authFields websocketAuth
	}

	WebsocketMessageType string
)

const (
	WebsocketRequestSubscribe   WebsocketRequestType = "subscribe"
	WebsocketRequestUnsubscribe WebsocketRequestType = "unsubscribe"

	WsChannelHeartbeat WebsocketChannelName = "heartbeat"
	WsChannelStatus    WebsocketChannelName = "status"
	WsChannelTicker    WebsocketChannelName = "ticker"
	WsChannelLevel2    WebsocketChannelName = "level2"
	WsChannelUser      WebsocketChannelName = "user"
	WsChannelMatches   WebsocketChannelName = "matches"
	WsChannelFull      WebsocketChannelName = "full"

	WsMessageSubscriptions WebsocketMessageType = "subscriptions"
	WsMessageHeartbeat     WebsocketMessageType = "heartbeat"
	WsMessageStatus        WebsocketMessageType = "status"
	WsMessageTicker        WebsocketMessageType = "ticker"
	WsMessageSnapshot      WebsocketMessageType = "snapshot"
	WsMessageL2Update      WebsocketMessageType = "l2update"
	WsMessageReceived      WebsocketMessageType = "received"
	WsMessageOpen          WebsocketMessageType = "open"
	WsMessageDone          WebsocketMessageType = "done"
	WsMessageMatch         WebsocketMessageType = "match"
	WsMessageChange        WebsocketMessageType = "change"
	WsMessageActivate      WebsocketMessageType = "activate"
)

func (wrt WebsocketRequestType) String() string {
	return string(wrt)
}

func (wcn WebsocketChannelName) String() string {
	return string(wcn)
}

func (wr WebsocketRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		WebsocketRequest
		websocketAuth
	}{
		wr,
		wr.authFields,
	})
}

func (wmt WebsocketMessageType) String() string {
	return string(wmt)
}
